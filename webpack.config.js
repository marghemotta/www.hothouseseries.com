const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const themePath = './wp-content/themes/gds-timber/';

var sitename = __dirname.split('/').pop();

module.exports = {
  mode: 'none',
  entry: [themePath+'static/site.js', themePath+'style.scss'],
  module: {
    rules: [
      {
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader,
        { loader: 'css-loader', options: { url: false, sourceMap: true } },
        { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(glsl|frag|vert)$/,
        loader: 'webpack-glsl-loader'
      }
    ]
  },
  plugins: [
      new MiniCssExtractPlugin({
        filename: "../style.css",
        // chunkFilename: "../[id].css"
      }),
      new BrowserSyncPlugin({
        proxy: 'http://localhost:8888/'+sitename+'/',
        files: [
          {
            match: [
              themePath+'**/*.php',
              themePath+'**/*.twig',
              themePath+'**/*.css',
              // themePath+'css/*.scss',
              themePath+'**/*.js'
            ],
            fn: function(event, file) {
              if (event === "change") {
                  const bs = require('browser-sync').get('bs-webpack-plugin');
                  bs.reload();
              }
            }
          }
        ]
      },
      {
        reload: false
      }),
      new UglifyJsPlugin({
        sourceMap: true
      }),
  ],
  output: {
    filename: 'site.min.js',
    path: path.resolve(__dirname, 'wp-content/themes/gds-timber/static')
  }
};
