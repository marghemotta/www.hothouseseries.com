(function($){
	$('#gds-cookie').hide();
	if( $.cookie("visited") != 'true' ) {
		$('#gds-cookie').show();
		$.cookie('visited', 'true', { expires: 15, path: '/' }); //cookie to be valid for entire site
	}
	$('.gds-cookie__close').click(function() {
		$('#gds-cookie').fadeOut(200);
		return false;
	});
})(jQuery);
