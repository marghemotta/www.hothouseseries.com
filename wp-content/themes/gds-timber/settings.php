<?php

/*
 0 - Script
*/
function gds_enqueue_scripts_theme_scripts() {
  wp_enqueue_script( 'main-script', get_template_directory_uri() . '/static/site.min.js?v=1.7', array( 'jquery' ), '1', true );
  wp_localize_script( 'main-script', 'drops', array(
    'theme_url' => get_template_directory_uri(),
  	'video' => get_field('video', 'option'),
  	'cover' => get_field('cover', 'option'),
  ));

  $imagesSizes = get_intermediate_image_sizes();
  $imagesSizesMDPos = array_search('medium_large', $imagesSizes);
  if ($imagesSizesMDPos) {
    array_splice($imagesSizes, $imagesSizesMDPos, 1);
  }
  $imagesSizesThumbnailPos = array_search('thumbnail', $imagesSizes);
  array_splice($imagesSizes, $imagesSizesThumbnailPos, 1);

  $imagesSizes[] = "original";
  wp_localize_script( 'main-script', 'media', array(
  	'imagesSizes' => $imagesSizes,
  ));

}
add_action('wp_enqueue_scripts', 'gds_enqueue_scripts_theme_scripts');

/*
 1 - Disable comments
*/

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
                if(post_type_supports($post_type, 'comments')) {
                        remove_post_type_support($post_type, 'comments');
                        remove_post_type_support($post_type, 'trackbacks');
                }
        }
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
        return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
        $comments = array();
        return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
        remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
                wp_redirect(admin_url()); exit;
        }
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
        if (is_admin_bar_showing()) {
                remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
}
add_action('init', 'df_disable_comments_admin_bar');

/*
 2 - Hide admin bar
*/
add_filter('show_admin_bar', '__return_false');

/*
 3 - Disable automatic responsive image Wordpress
*/

// Clean the up the image from wp_get_attachment_image()
add_filter( 'wp_get_attachment_image_attributes', function( $attr )
{
    if( isset( $attr['sizes'] ) )
        unset( $attr['sizes'] );

    if( isset( $attr['srcset'] ) )
        unset( $attr['srcset'] );

    return $attr;

 }, PHP_INT_MAX );

// Override the calculated image sizes
add_filter( 'wp_calculate_image_sizes', '__return_false',  PHP_INT_MAX );

// Override the calculated image sources
add_filter( 'wp_calculate_image_srcset', '__return_false', PHP_INT_MAX );

// Remove the reponsive stuff from the content
remove_filter( 'the_content', 'wp_make_content_images_responsive' );

/*
 4 - Rename images with sitename and postname
*/

add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

function custom_upload_filter( $file ){
  global $post;

  $site_name = sanitize_title(get_bloginfo( 'name' ))."_";
  $file_type = wp_check_filetype($file['name']);
  $file_ext = '.'.$file_type['ext'];
  $file_name = basename($file['name'], $file_ext);

  if ( isset( $_REQUEST['post_id'] ) ) {
     $post_id  = absint( $_REQUEST['post_id'] );
     $post_obj  = get_post( $post_id );
     $post_slug = sanitize_title($post_obj->post_title).'_';
  }

  $file['name'] = $site_name.$post_slug.$file_name.$file_ext;
   return $file;
}

/*
 5 - Responsive Image
*/

add_image_size( 'xxs', 320, 9999, false );
add_image_size( 'xs', 420, 9999, false );
add_image_size( 's', 600, 9999, false );
add_image_size( 'medium', 1000, 9999, false );
add_image_size( 'large', 1400, 9999, false );
add_image_size( 'xl', 2000, 9999, false );
add_image_size( 'xxl', 3000, 9999, false );

function gds_enqueue_scripts_responsive_images() {
  // wp_enqueue_script( 'image-script', get_template_directory_uri() . '/static/responsive.js?v=1.7', array( 'jquery' ), '1', true );
  // $imagesSizes = get_intermediate_image_sizes();
  // $imagesSizesMDPos = array_search('medium_large', $imagesSizes);
  // if ($imagesSizesMDPos) {
  //   array_splice($imagesSizes, $imagesSizesMDPos, 1);
  // }
  // $imagesSizesThumbnailPos = array_search('thumbnail', $imagesSizes);
  // array_splice($imagesSizes, $imagesSizesThumbnailPos, 1);
  //
  // $imagesSizes[] = "original";
  // wp_localize_script( 'image-script', 'media', array(
  // 	'imagesSizes' => $imagesSizes,
  // ));
}

function gds_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
  if (isset($attachment[0])) {
    return $attachment[0];
  }
}

function gds_responsive_image_filter($content) {
  $re = '/(<img [^>]*>)/m';
  $result = preg_replace_callback($re, function($matches) {
    $current_image = $matches[1];
    $current_image_arr = array();
    // error_log(print_r($current_image, true));
    preg_match( '/src="([^"]*)"/i', $current_image, $current_image_arr );
    $current_image_url = $current_image_arr[1];
    $current_image_id = gds_get_image_id($current_image_url); // Works only with fullsize images
    if (!$current_image_id) {
      $current_image_url_arr = explode("-", $current_image_url);
      $dimension_and_extension = $current_image_url_arr[count($current_image_url_arr) - 1];
      $dimension_and_extension_arr = explode(".", $dimension_and_extension);
      $extension = ".".$dimension_and_extension_arr[1];
      array_splice($current_image_url_arr, count($current_image_url_arr) - 1, 1);
      $current_image_url_arr[count($current_image_url_arr) - 1] = $current_image_url_arr[count($current_image_url_arr) - 1].$extension;
      $current_image_url_no_size = join("-", $current_image_url_arr);
      $current_image_id = gds_get_image_id($current_image_url_no_size);
    }
    $new_image = "<img ".gds_responsive_image($current_image_id, true).">";
    return $new_image;
  }, $content);

  return $result;
}

add_filter( 'the_content', 'gds_responsive_image_filter' );

add_action('wp_enqueue_scripts', 'gds_enqueue_scripts_responsive_images');

function gds_responsive_image ($img, $meta = true) {
	$dataW = "";
	$dataH = "";
	$dataS = "";
  $image_loop_count = 0;
  if (is_numeric($img)) {
    // 134
    $id_image = $img;
    /*
    array (
      'width' => 3000,
      'height' => 4500,
      'file' => '2018/05/raumplan_quattroperzero_raumplan-quattroperzero_02-1.jpg',
      'sizes' =>
      array (
        'thumbnail' =>
        array (
          'file' => 'raumplan_quattroperzero_raumplan-quattroperzero_02-1-150x150.jpg',
          'width' => 150,
          'height' => 150,
          'mime-type' => 'image/jpeg',
        ),
    */
    $img_original = get_post_meta($id_image, "_wp_attached_file", true);
    $upload_dir = wp_upload_dir();
    $basename = basename($img_original);
    $main_url_dir = str_replace($basename, "", $img_original);
    $main_url = $upload_dir['baseurl']."/".$main_url_dir;

    $img_obj = get_post_meta($id_image, "_wp_attachment_metadata", true);
    $img_sizes_obj = $img_obj['sizes'];

    foreach ($img_sizes_obj as $img_sizes_key => $img_sizes_value) {
			if ($img_sizes_key != "thumbnail" && $img_sizes_key != "medium_large") {
        if ($image_loop_count == 0) {
          $dataSrc = "src='".$main_url.$img_sizes_value['file']."' ";
        }
        $image_loop_count++;
				$dataW .= "data-width-".$img_sizes_key."='".$img_sizes_value['width']."' ";
				$dataH .= "data-height-".$img_sizes_key."='".$img_sizes_value['height']."' ";
				$dataS .= "data-src-".$img_sizes_key."='".$main_url.$img_sizes_value['file']."' ";
			}
		}
		$img_sizes_key = "original";
		$dataW .= "data-width-".$img_sizes_key."='4000' ";
		$dataH .= "data-height-".$img_sizes_key."='4000' ";
		$dataS .= "data-src-".$img_sizes_key."='".$upload_dir['baseurl'].'/'.$img_obj['file']."' ";
  } elseif (is_object($img)) {
		/*
    [medium] => Array
        (
            [file] => Still_six_gallery_017-600x900.jpg
            [width] => 600
            [height] => 900
            [mime-type] => image/jpeg
        )
			*/

    $id_image = gds_get_image_id($img->guid);

		$main_img = basename($img->guid);
		$main_url = str_replace($main_img, "", $img->guid);

		foreach ($img->sizes as $img_sizes_key => $img_sizes_value) {
			if ($img_sizes_key != "thumbnail" && $img_sizes_key != "medium_large") {
        if ($image_loop_count == 0) {
          $dataSrc = "src='".$main_url.$img_sizes_value['file']."' ";
        }
        $image_loop_count++;
				$dataW .= "data-width-".$img_sizes_key."='".$img_sizes_value['width']."' ";
				$dataH .= "data-height-".$img_sizes_key."='".$img_sizes_value['height']."' ";
				$dataS .= "data-src-".$img_sizes_key."='".$main_url.$img_sizes_value['file']."' ";
			}
		}
		$img_sizes_key = "original";
		$dataW .= "data-width-".$img_sizes_key."='4000' ";
		$dataH .= "data-height-".$img_sizes_key."='4000' ";
		$dataS .= "data-src-".$img_sizes_key."='".$img->guid."' ";

	} else {
		/*
		[sizes] => Array
	    (
	    [thumbnail] => http://.../wp-content/uploads/2017/07/8-l-150x150.jpg
	    [thumbnail-width] => 150
	    [thumbnail-height] => 150
			*/
		if ($img['sizes']) {

      $id_image = gds_get_image_id($img['url']);

			foreach ($img['sizes'] as $img_sizes_key => $img_sizes_value) {
				if (strpos($img_sizes_key, 'width') === false && strpos($img_sizes_key, 'height') === false) {
					if ($img_sizes_key != "thumbnail" && $img_sizes_key != "medium_large") {
            if ($image_loop_count == 0) {
              $dataSrc = "src='".$img['sizes'][$img_sizes_key]."' ";
            }
            $image_loop_count++;
						$dataW .= "data-width-".$img_sizes_key."='".$img['sizes'][$img_sizes_key.'-width']."' ";
						$dataH .= "data-height-".$img_sizes_key."='".$img['sizes'][$img_sizes_key.'-height']."' ";
						$dataS .= "data-src-".$img_sizes_key."='".$img['sizes'][$img_sizes_key]."' ";
					}
				}
			}
			$img_sizes_key = "original";
			$dataW .= "data-width-".$img_sizes_key."='".$img['width']."' ";
			$dataH .= "data-height-".$img_sizes_key."='".$img['height']."' ";
			$dataS .= "data-src-".$img_sizes_key."='".$img['url']."' ";
		}
	}

  $meta_data = "";
  // Add meta
  if ($meta && $id_image) {
    $image = get_post($id_image);
    $image_title = $image->post_title;
    $image_caption = $image->post_excerpt;
    // $image_description = $image->post_content;
    $image_alt = get_post_meta($id_image, '_wp_attachment_image_alt', true);
    if ($image_title) {
      $meta_data .= "title= '".$image_title."'";
    }
    if ($image_caption) {
      $meta_data .= "caption='".$image_caption."'";
    }
    if ($image_alt) {
      $meta_data .= " alt='".$image_alt."' ";
    }
  }

	return $dataW." ".$dataH." ".$dataS." data-responsive='true' ".$meta_data;
}

function gds_acf_field_content( $value, $post_id, $field ) {
  if ($field['type'] == "wysiwyg" && !is_admin()) {
    $value = apply_filters('the_content', $value);
    $value = preg_replace('/(<figur[^>]+) style=".*?"/i', '$1', $value);
  }
  return $value;
}

// acf/load_value - filter for every value load
add_filter('acf/load_value', 'gds_acf_field_content', 10, 3);

/*
 *  Clear image in editor
 */

 add_filter( 'post_thumbnail_html', 'gds_remove_wh_editor', 10 );
 add_filter( 'image_send_to_editor', 'image_add_caption', 20, 8 );

 function gds_remove_wh_editor( $html ) {
	$html = preg_replace( '/(width|height|alt)="\d*"\s/', "", $html );
	$html = preg_replace('/class=".*?"/', '', $html);
	if( current_theme_supports( 'html5' )  && ! $caption ) {
    if ($caption) {
      $html = sprintf( '<figure>%s<figcaption>'.$caption.'</figcaption></figure>', $html );
    } else {
      $html = sprintf( '<figure>%s</figure>', $html );
    }
 	}
  return $html;
 }

/*
 6 - Modernizr
 */

 function gds_enqueue_scripts_modernizr() {
   wp_enqueue_script( 'modernizr-script', get_template_directory_uri() . '/static/gds-modernizr.min.js?v=1.7', array( 'jquery' ), '1', true );
 }

add_action('wp_enqueue_scripts', 'gds_enqueue_scripts_modernizr');

?>
