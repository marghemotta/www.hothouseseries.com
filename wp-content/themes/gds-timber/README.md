# GDS Timber

## Install

``` bash

# install all npms
mv wp-content/themes/gds-timber/.gitignore .gitignore
mv wp-content/themes/gds-timber/package.json package.json
mv wp-content/themes/gds-timber/webpack.config.js webpack.config.js
npm install
```

## Work

``` bash

# normal one
npm start

```
