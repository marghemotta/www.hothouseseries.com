require('./drops/index3.js');
require('./drops/appanno.js');
require('./responsive.js');

jQuery(document).ready(function($) {


    // iPhone
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) {
      document.querySelector('html').classList.add('iphone')
    }

    // Slider

    if (document.querySelector(".events__slider-in-page")) {
        var sliderInPage = tns({
            container: '.events__slider-in-page',
            items: 1,
            slideBy: 'page',
            // mode: "gallery",
            nav: false,
            speed: 0,
            controls: true,
        });
        document.querySelector('.events__slider-in-page-wrapper').addEventListener('click', function () {
          if (document.querySelector('.events__slider-in-page-wrapper--not-active')) {
            sliderInPage.goTo(1);
            document.querySelector('.events__slider-in-page-wrapper--not-active').classList.remove('events__slider-in-page-wrapper--not-active');
          }
        });
    }

    if (document.querySelector(".events__slider")) {
        var slider = tns({
            container: '.events__slider',
            items: 1,
            slideBy: 'page',
            mode: "gallery",
            nav: false,
            speed: 0,
            controls: false,
        });

    }

    if (document.querySelector(".image_single")) {

        // close slider
        document.querySelector('.close_button').onclick = function() {
            document.querySelector(".lightbox").classList.remove("visible");
        }

        //next prev
        document.querySelector(".slider__wrapper").addEventListener("click", function(e) {
            if (e.screenX > window.innerWidth / 2) {
                slider.goTo('next');
                changeSliderNumber()
            } else {
                slider.goTo('prev');
                changeSliderNumber()
            }
        })

        //change slider text
        function changeSliderNumber() {
            var info = slider.getInfo(),
                indexCurrent = info.index;
            indexTotal = info.slideCount;
            indexCurrent++
            if (indexCurrent > indexTotal) {
                indexCurrent = 1
            }
            document.getElementById("current_slide").innerHTML = indexCurrent + "/" + indexTotal;
        }

        //partendo dalla foto
        document.querySelectorAll('.image_single').forEach(function(item) {
            item.onclick = function() {
              document.querySelector(".lightbox").classList.add("visible");
              var target = this.dataset.target;
              slider.goTo(target);
              changeSliderNumber()
              window.dispatchEvent(new Event('resize'));
            }
            item.onmouseover = function() {
              var texttarget = document.getElementById(this.dataset.target)
              var backtarget = document.getElementById(this.dataset.back)
              texttarget.classList.add("colored");
              backtarget.classList.add("image_single_wrapper2_back");
            }
            item.onmouseout = function() {
              var texttarget = document.getElementById(this.dataset.target)
              var backtarget = document.getElementById(this.dataset.back)
              texttarget.classList.remove("colored");
              backtarget.classList.remove("image_single_wrapper2_back");
            }
        })

        //partendo dal testo

        document.querySelectorAll('.image_desc_wrapper').forEach(function(item) {
            item.onclick = function() {
                document.querySelector(".lightbox").classList.add("visible");
                var target = this.dataset.target;
                slider.goTo(target);
                changeSliderNumber()
            }
            item.onmouseover = function() {
                var texttarget = document.getElementById(this.dataset.target)
                var backtarget = document.getElementById(this.dataset.back)
                var imgtarget = document.getElementById(this.dataset.mix)
                this.classList.add("colored");
                imgtarget.classList.add("image_single_mix");
                backtarget.classList.add("image_single_wrapper2_back");
            }
            item.onmouseout = function() {
                var texttarget = document.getElementById(this.dataset.target)
                var backtarget = document.getElementById(this.dataset.back)
                var imgtarget = document.getElementById(this.dataset.mix)
                imgtarget.classList.remove("image_single_mix");
                this.classList.remove("colored");
                backtarget.classList.remove("image_single_wrapper2_back");
            }
        })
    }

    // Video

    if (document.querySelector('.bigger_button')) {

      document.querySelector('.bigger_button').onclick = function() {
        document.querySelector('.video__controls').removeAttribute('style')
        document.querySelector('.video__wrapper').classList.add("video__shifting");
        document.querySelector('.video__wrapper').classList.toggle("video__shift");
      }
    }

    if (document.querySelector('.play_button')) {
      var myVideo = document.getElementById("event__video");

      function playPause() {
          if (myVideo.paused){
            myVideo.play();
            if (!document.querySelector('html').classList.contains('iphone')) {
              document.getElementById("play_button").innerHTML = "Pause";
            }
          }
          else{
            myVideo.pause();
            document.getElementById("play_button").innerHTML = "Play";

          }
      }

      document.querySelector('.play_button').onclick = function() {
          playPause()
      }
    }

    // Size of container
    const videoAnimationSize = function () {
      if (document.querySelector('.video__wrapper').classList.contains('video__shift')) {
        let videoRatio = document.querySelector('.video__video').videoWidth / document.querySelector('.video__video').videoHeight
        let containerRatio = document.querySelector('.video__relative').offsetWidth / document.querySelector('.video__relative').offsetHeight
        if (containerRatio > videoRatio) {
          let newWidth = document.querySelector('.video__relative').offsetHeight*videoRatio
          let paddingNew = document.querySelector('.video__relative').offsetWidth - newWidth
          document.querySelector('.video__controls').style.width = `${newWidth}px`;
          document.querySelector('.video__controls').style.paddingLeft = `${paddingNew}px`;
        } else {
          document.querySelector('.video__controls').removeAttribute('style')
        }
      } else {
        document.querySelector('.video__controls').removeAttribute('style')
      }
      setTimeout(function () {
        document.querySelector('.video__wrapper').classList.remove("video__shifting")
      }, 100)
    }
    document.querySelector('.video__wrapper').addEventListener("transitionend", videoAnimationSize);

    // close on <esc>
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var isEscape = false;
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc");
        } else {
            isEscape = (evt.keyCode === 27);
        }
        if (isEscape) {
            if (document.querySelector('.lightbox.visible') && document.querySelector('.close_button')) {
                document.querySelector('.close_button').click()
            }
            if (document.querySelector('.video__wrapper').classList.contains("video__shift")) {
              document.querySelector('.bigger_button').click()
            }
        }
    };

});
