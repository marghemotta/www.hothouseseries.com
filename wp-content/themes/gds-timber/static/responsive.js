jQuery( document ).ready( function( $ ) {

  var ua = navigator.userAgent || navigator.vendor || window.opera;
  var isInstagram = (ua.indexOf('Instagram') > -1) ? true : false;

  if (document.documentElement.classList ){
  	if (isInstagram) {
  		window.document.body.classList.add('img-responsive-fallback');
  	}
  }

  // Responsive Image
  function isRectangleSmallerThanRectangle(r1, r2) {
    // Image case with height = 0;
    // console.log(r1, r2);
    if (r1[1] == 0) {
      if (r1[0] < r2[0] && r1[1] < r2[1]) {
        return true;
      } else {
        return false;
      }
    } else {
      if (r1[0] < r2[0]) {
        return true;
      } else {
        return false;
      }
    }
  }

  function getResponsiveImage(_this, isImage) {
    // Check if parent is <figure>
    var isFigure = _this.parent()[0].tagName.toLowerCase() == "figure";
    var imageToCheck = _this;
    if (isFigure) {
      imageToCheck = _this.parent();
    }
    var imageWidth = imageToCheck.width();
    var imageHeight = imageToCheck.height();
    if (isImage) {
      var ratio = _this.data('height-xxs') / _this.data('width-xxs');
      var imageHeight = imageWidth * ratio;
    }
    var imageRectangle = Array(imageWidth, imageHeight);
    if (isRetina()) {
      imageRectangle = Array(imageWidth*2, imageHeight*2);
    }
    var correctSize = null;
    for (var i = 0; i < media.imagesSizes.length; i++) {
      var currentSize = media.imagesSizes[i].toLowerCase();
      var responsiveImageRectangle = Array(_this.data('width-'+currentSize), _this.data('height-'+currentSize));
      if (isRectangleSmallerThanRectangle(imageRectangle, responsiveImageRectangle) && correctSize == null) {
        correctSize = currentSize;
        _this.data('currentSize', correctSize);
      }
    }
    if (!correctSize) {
      correctSize = 'original';
    }
    return correctSize;
  }

  function responsiveImage() {
    if (document.querySelector('body').classList.contains('img-responsive-fallback')) {
      $("[data-responsive='true']").each(function () {
        $(this)[0].src = $(this).data("src-original")
      });
      return
    }
    setTimeout(function () {
      $("[data-responsive='true']").each(function () {
        var isImage = $(this)[0].tagName.toLowerCase() == "img";
        var imageRectangle = Array($(this).width(), $(this).height());
        if (isRetina()) {
          imageRectangle = Array($(this).width()*2, $(this).height()*2);
        }
        // Check if has not already a value
        if (!$(this).data('currentSize')) {
          correctSize = getResponsiveImage($(this), isImage);
          if ($(this)[0].tagName.toLowerCase() == "img") {
            $(this).attr("src", $(this).data('src-'+correctSize));
          } else {
            $(this).css("background-image", "url('"+$(this).data('src-'+correctSize)+"')");
          }
        } else {
          var previousSize = $(this).data('currentSize');
          var previousImageRectangle = Array($(this).data('width-'+previousSize), $(this).data('height-'+previousSize));
          if (isRectangleSmallerThanRectangle(previousImageRectangle, imageRectangle)) {
            correctSize = getResponsiveImage($(this), isImage);
            if (isImage) {
              $(this).attr("src", $(this).data('src-'+correctSize));
            } else {
              $(this).css("background-image", "url('"+$(this).data('src-'+correctSize)+"')");
            }
          }
        }
      });
    }, 10);
  }

  function isRetina(){
    var isRetina = ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio >= 2)) && /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
    if (!isRetina) {
      isRetina = (window.devicePixelRatio >= 2);
    }
    return isRetina
  }

  // Begin!
  responsiveImage();

  // Bind
  $( window ).resize(function() {
    responsiveImage();
  })

});
