var canvas = document.getElementById("drawCanvas");
if (canvas) {
  var ctx = canvas.getContext("2d");
  var prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;
  var mouseDown = false;
  var firstDown = true;
  var canvasScale = 2;
  var brushSize = 80;

  // onWindowResize();

  var bgImg = new Image(40, 40);
  bgImg.src = drops.theme_url+"/assets/drops/bg_4.jpg";
  bgImg.onload = function() {
    onWindowResize();
  }

  console.log("sono un canvas");

  function draw(e) {
    if (!mouseDown) {
      // firstDown = true;
      return;
    }

    prevX = currX;
    prevY = currY;
    currX = (e.clientX - canvas.offsetLeft) / canvasScale;
    currY = (e.clientY - canvas.offsetTop) / canvasScale;

    console.log(firstDown);

    var currentPoint = {
      x: currX,
      y: currY
    };
    var dist = distanceBetween(lastPoint, currentPoint);
    var angle = angleBetween(lastPoint, currentPoint);

    if (firstDown) {
      prevX = currX;
      prevY = currY;
      firstDown = false;
    }

    // ctx.beginPath();
    // ctx.moveTo(prevX,prevY);
    // ctx.lineTo(currX,currY);
    // ctx.stroke();

    for (var i = 0; i < dist; i += 5) {

      x = lastPoint.x + (Math.sin(angle) * i);
      y = lastPoint.y + (Math.cos(angle) * i);

      var radgrad = ctx.createRadialGradient(x, y, 10, x, y, 20);

      radgrad.addColorStop(0, '#000');
      radgrad.addColorStop(0.5, 'rgba(0,0,0,0.5)');
      radgrad.addColorStop(1, 'rgba(0,0,0,0)');

      ctx.fillStyle = radgrad;
      ctx.fillRect(x - 20, y - 20, 40, 40);
    }

    lastPoint = currentPoint;

    // var radgrad = ctx.createRadialGradient(currX,currY,10,currX,currY,20);
    //
    // radgrad.addColorStop(0, '#000');
    // radgrad.addColorStop(0.5, 'rgba(0,0,0,0.3)');
    // radgrad.addColorStop(1, 'rgba(0,0,0,0)');
    // ctx.fillStyle = radgrad;
    //
    // ctx.fillRect(currX-20, currY-20, 40, 40);

  }

  function distanceBetween(point1, point2) {
    return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
  }

  function angleBetween(point1, point2) {
    return Math.atan2(point2.x - point1.x, point2.y - point1.y);
  }

  window.addEventListener('resize', onWindowResize, false);

  canvas.addEventListener("mousemove", function(e) {
    draw(e)
  }, false);
  canvas.addEventListener("mousedown", function(e) {
    mouseDown = true;
    lastPoint = {
      x: (e.clientX - canvas.offsetLeft) / canvasScale,
      y: (e.clientY - canvas.offsetTop) / canvasScale
    };
    draw(e)
  }, false);
  canvas.addEventListener("mouseup", function(e) {
    mouseDown = false;
    firstDown = true;
  }, false);
  canvas.addEventListener("mouseout", function(e) {
    mouseDown = false;
    firstDown = true;
  }, false);

  canvas.addEventListener("touchstart", function(e) {
    mouseDown = true;
    lastPoint = {
      x: (e.clientX - canvas.offsetLeft) / canvasScale,
      y: (e.clientY - canvas.offsetTop) / canvasScale
    };
    draw(e.touches[0]);
  }, false);
  canvas.addEventListener("touchmove", function(e) {
    draw(e.touches[0]);
  }, false);
  canvas.addEventListener("touchend", function(e) {
    mouseDown = false;
    firstDown = true;
  }, false);
  canvas.addEventListener("touchcancel", function(e) {
    mouseDown = false;
    firstDown = true;
  }, false);

  function onWindowResize() {
    // console.log();
    canvas.width = window.innerWidth / canvasScale;
    canvas.height = window.innerHeight / canvasScale;

    // ctx.fillStyle = 'rgba(255,255,255,1)';
    // ctx.fillRect(0,0,canvas.width,canvas.height)

    // pat = ctx.createPattern(bgImg, "repeat");
    // ctx.rect(0, 0, canvas.width, canvas.height);
    // ctx.fillStyle = pat;
    // ctx.fill();

    fillPattern(bgImg, 128, 128)

    // ctx.globalCompositeOperation = 'destination-out';
    // ctx.globalCompositeOperation = 'screen';

    ctx.lineWidth = brushSize / canvasScale;
    ctx.strokeStyle = '#ffffff';
    ctx.lineCap = 'round';
    // console.log("resized");
  }

  function fillPattern(img, w, h) {
    //draw once
    ctx.drawImage(img, 0, 0, w, h);

    while (w < canvas.width) {
      ctx.drawImage(canvas, w, 0);
      w <<= 1; // shift left 1 = *2 but slightly faster
    }
    while (h < canvas.height) {
      ctx.drawImage(canvas, 0, h);
      h <<= 1;
    }
  }

}
