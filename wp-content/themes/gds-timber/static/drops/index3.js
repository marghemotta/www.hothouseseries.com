// import 'core-js';
import RainRenderer from "./rain-renderer";
import Raindrops from "./raindrops";
import loadImages from "./image-loader";
import createCanvas from "./create-canvas";
import TweenLite from "gsap";

var canvasScale = 2;
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

function setCookie(cname, cvalue, exseconds) {
  var d = new Date();
  d.setTime(d.getTime() + (exseconds * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function generateHTML(force) {
  // let drop_muted = ''
  // if (window.innerWidth < 1100) {
  //   drop_muted = 'muted'
  // }
  // let video_url = drops.video;
  // let video_poster = drops.cover;
  // let cookie = getCookie('dropsSeen');
  //
  // let drop_html = `
  // <div class="drop__wrapper">
  //   <div class="drop__loader"></div>
  //   <div class="drop__button">HotHouse</div>
  //   <video class="videobg" src="${video_url}" autoplay loop ${drop_muted} poster="${video_poster}"></video>
  //   <canvas id="container" width="1" height="1" style="position:absolute"></canvas>
  //   <div class="drop__drawer">
  //     <canvas id="drawCanvas" width="300" height="300"></canvas>
  //   </div>
  // </div>`
  //
  // if ((document.querySelector('.footer__wrapper') && !document.querySelector('.drop__wrapper') && !cookie) || force) {
  //   document.querySelector('.footer__wrapper').innerHTML = drop_html
  //
  //   let video = document.querySelector('.videobg')
  //

  if (document.querySelector('.drop__button')) {
    // let video = document.querySelector('.videobg')
    // var b = setInterval(()=>{
    //   if(video.readyState >= 3){
    //     const playPromise = video.play();
    //     if (playPromise !== null){
    //         playPromise.catch(() => {
    //           setTimeout(function () {
    //             video.play();
    //           }, 400)
    //         })
    //     }
    //     clearInterval(b);
    //   }
    // }, 500);

    function removeElementFromDOM(elem) {
      elem.parentNode.removeChild(elem);
    }

    // Fix Scrolling
    document.querySelector('html').classList.add('html--no-scroll')
    if (document.querySelector('.video__wrapper')) {
      document.querySelector('.video__wrapper').classList.add('video__wrapper--hidden')
    }
    // Set Click
    // document.querySelector('.drop__drawer').addEventListener('click', function () {
    //   let videobig = document.querySelector('.videobg')
    //   videobig.muted = false
    // })
    document.querySelector('.drop__button').addEventListener('click', function () {
      document.querySelector('.drop__wrapper').classList.add('drop__wrapper--hidden')
      document.querySelector('html').classList.remove('html--no-scroll')
      window.dispatchEvent(new Event('resize'));
      setTimeout(function () {
        removeElementFromDOM(document.querySelector('.drop__wrapper'))
        if (document.querySelector('.video__wrapper')) {
          document.querySelector('.video__wrapper').classList.remove('video__wrapper--hidden')
        }
      }, 400)
    });

    // setCookie('dropsSeen', true, 60);
  }

}

window.addEventListener('beforeunload', (event) => {
  setCookie('dropsSeen', true, 1);
  // event.preventDefault();
  return undefined;
});

function removeElementFromDOM(elem) {
  elem.parentNode.removeChild(elem);
}

function get_browser() {
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'IE',version:(tem[1]||'')};
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR|Edge\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }

generateHTML(false);

if (document.querySelector('.drop__wrapper')) {

  let videoFg, videoBg=document.querySelector(".videobg");

  let dropColor, dropAlpha;
  let fallback = iOS;
  let browser = get_browser()
  if (browser.name == "Safari") {
    // if (Number(browser.version)>11) {
      fallback = true
    // }
  }

  let textureFg,
    textureFgCtx,
    textureBg,
    textureBgCtx;

  let textureBgSize={
    width:456,
    height:256
  }
  let textureFgSize={
    width:114,
    height:64
  }
  // console.log("NO SARCOPE");
  // let textureBgSize={
  //   width:384,
  //   height:256
  // }
  // let textureFgSize={
  //   width:96,
  //   height:64
  // }


  let raindrops,
    renderer,
    canvas;

  let parallax={x:0,y:0};

  let video_poster = drops.cover;

  if(fallback) { // carica immagine al posto di video
    loadImages([
      {name:"bgImage",src:video_poster}
    ]).then((images)=>{
      videoBg = images.bgImage.img;
      videoFg = images.bgImage.img;

      loadTextures();
    });
  } else {
    loadTextures();
  }

  function loadTextures(){
    loadImages([
      {name:"dropAlpha",src:drops.theme_url+"/assets/drops/drop-alpha.png"},
      {name:"dropColor",src:drops.theme_url+"/assets/drops/drop-color.png"},
    ]).then((images)=>{
      dropColor = images.dropColor.img;
      dropAlpha = images.dropAlpha.img;

      init(false);
    });
  }



  function init(resize) {
    canvas=document.querySelector('#container');

    let dpi=window.devicePixelRatio;
    canvas.width=window.innerWidth * dpi * 0.75;
    canvas.height=window.innerHeight * dpi * 0.75;
    canvas.style.width=window.innerWidth+"px";
    canvas.style.height=window.innerHeight+"px";

    raindrops = new Raindrops(
      canvas.width,
      canvas.height,
      dpi,
      dropAlpha,
      dropColor,
      {
        minR:20,
        maxR:40,
        rainChance: 0.05,
        collisionRadiusIncrease:0.002,
        dropletsRate:35,
        dropletsSize:[3,7.5],
        dropletsCleaningRadiusMultiplier:0.30,
        initialDroplets: 30000,
        initialDrops: 20,
        trailRate:30
      }
    );


    textureFg = createCanvas(textureFgSize.width,textureFgSize.height);
    textureFgCtx = textureFg.getContext('2d');
    textureBg = createCanvas(textureBgSize.width,textureBgSize.height);
    textureBgCtx = textureBg.getContext('2d');

    generateTextures();

    renderer = new RainRenderer(canvas, raindrops.canvas, textureFg, textureBg, null,{
      brightness:1.1,
      alphaMultiply:6,
      alphaSubtract:3,
      parallaxFg:40
    });

    if (!resize) {

      setupEvents();

      if (document.querySelector('.drop__loader')) {
        setTimeout(function () {
          document.querySelector('.drop__loader').classList.add('drop__loader--hidden')
        }, 400)
      }
    } else {
      // textureFg = null
      // textureBg = null
      // // removeElementFromDOM(textureFg)
      // // removeElementFromDOM(textureBg)
      // // console.log(textureFg);
      // // console.log(textureBg);
    }

    // console.log("LOADED");
  }

  function setupEvents(){
    updateTextures();
    // setupParallax();
  }
  function setupParallax(){
    document.addEventListener('mousemove',(event)=>{
      let x=event.pageX;
      let y=event.pageY;

      TweenLite.to(parallax,1,{
        x:((x/canvas.width)*2)-1,
        y:((y/canvas.height)*2)-1,
        ease:Quint.easeOut,
        onUpdate:()=>{
          renderer.parallaxX=parallax.x;
          renderer.parallaxY=parallax.y;
        }
      })
    });
  }
  function updateTextures(){
    generateTextures();
    renderer.updateTextures();

    requestAnimationFrame(updateTextures);
  }
  function generateTextures(){
    if(fallback) {
      textureFgCtx.drawImage(videoBg,0,0,textureFgSize.width,textureFgSize.height);
      textureBgCtx.drawImage(videoBg,0,0,textureBgSize.width,textureBgSize.height);
    } else {
      textureFgCtx.drawImage(videoBg,0,textureBgSize.height,textureFgSize.width,textureFgSize.height,0,0,textureFgSize.width,textureFgSize.height);
      textureBgCtx.drawImage(videoBg,0,0,textureBgSize.width-1,textureBgSize.height-1,0,0,textureBgSize.width,textureBgSize.height);
    }

  }

  // Resize
  let currentWindowSize = [window.innerHeight, window.innerWidth];
  setTimeout(function () {
    window.addEventListener('resize', function () {
      if (window.innerHeight > currentWindowSize[0] || window.innerWidth > currentWindowSize[1]) {
        if (document.querySelector('.drop__button')) {
          document.querySelector('.drop__button').click()
        }
      } else {
        currentWindowSize = [window.innerHeight, window.innerWidth];
      }
    }, false);
  }, 800)

}

// Inactivity

// var inactivityTime = function () {
//   var time;
//   window.onload = resetTimer;
//   // DOM Events
//   document.onmousemove = resetTimer;
//   document.onkeypress = resetTimer;
//
//   function reinit() {
//     console.log('reinit');
//     if (!document.querySelector('.drop__wrapper')) {
//       generateHTML(true)
//       init()
//     }
//   }
//
//   function resetTimer() {
//     console.log('resetTimer');
//     clearTimeout(time);
//     time = setTimeout(reinit, 3000)
//     // 1000 milliseconds = 1 second
//   }
// };
// inactivityTime();
