<?php

include 'settings.php';

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Homepage',
		'menu_title'	=> 'Homepage',
		'menu_slug' 	=> 'homepage',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
		register_post_type( 'events',
			array(
				'labels' => array(
					'name' => __( 'Events' ),
					'singular_name' => __( 'Event' )
				),
				'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
				'public' => true,
				'has_archive' => true,
				'menu_position' => 1,
			)
		);
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		$context['main_menu'] = new TimberMenu('main_menu');
		$context['site'] = $this;
		$context['theme_url'] = get_template_directory_uri();
		$context['drop_video'] = get_field('video', 'option');
		$context['drop_cover'] = get_field('cover', 'option');
		$context['drop_muted'] = "muted";
		$context['about_photo'] = get_field('about_photo', 'option');
	  $context['drop_cookie'] = $_COOKIE['dropsSeen'];

		if( $iPod || $iPhone ) {
			$context['drop_muted'] = "muted";
		} else if($iPad) {
			$context['drop_muted'] = "muted";
		} else if($Android) {
		} else if($webOS) {
		}

		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		return $twig;
	}

}


new StarterSite();
